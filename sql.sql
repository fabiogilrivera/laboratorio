
/****** Object:  Database [perceptor]    Script Date: 25/07/2022 6:01:09 p. m. ******/


/**USE [perceptor]**/


CREATE TABLE [dbo].[Content](
	[ContentId] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](1024) NOT NULL,
	[Level] [int] NOT NULL,
	[Enable] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyBy] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Course] PRIMARY KEY CLUSTERED 
(
	[ContentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Level]    Script Date: 25/07/2022 6:01:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Level](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Order] [int] NOT NULL,
	[Enable] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyBy] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Level] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Linkedin]    Script Date: 25/07/2022 6:01:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Linkedin](
	[employee_number] [nvarchar](max) NULL,
	[Nombre] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[Nombre_del_contenido] [nvarchar](max) NULL,
	[Proveedor_de_contenido] [nvarchar](max) NULL,
	[Tipo_de_contenido] [nvarchar](max) NULL,
	[ID_del_contenido] [nvarchar](max) NULL,
	[Horas_de_visualizacion] [nvarchar](max) NULL,
	[Porcentaje_de_finalizacion] [nvarchar](max) NULL,
	[Inicio] [nvarchar](max) NULL,
	[Última_visualizacion] [nvarchar](max) NULL,
	[Finalizacion] [nvarchar](max) NULL,
	[Total_de_evaluaciones] [nvarchar](max) NULL,
	[N_de_evaluaciones_completadas] [nvarchar](max) NULL,
	[Conocimientos_y_aptitudes] [nvarchar](max) NULL,
	[Nombre_del_curso] [nvarchar](max) NULL,
	[ID_del_curso] [nvarchar](max) NULL,
	[department] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 25/07/2022 6:01:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](max) NULL,
	[descripcion] [nvarchar](max) NULL,
	[valor] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SCacademy]    Script Date: 25/07/2022 6:01:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SCacademy](
	[Nempleado] [nvarchar](max) NULL,
	[IdUsuario] [nvarchar](max) NULL,
	[Apellidos] [nvarchar](max) NULL,
	[Nombres] [nvarchar](max) NULL,
	[TitulodelModulo] [nvarchar](max) NULL,
	[Fechadeinscripcion] [nvarchar](max) NULL,
	[Fechadeinicio] [nvarchar](max) NULL,
	[Fechadefinalizacion] [nvarchar](max) NULL,
	[FechadefinalizacionPropiedades] [nvarchar](max) NULL,
	[Estadodelasesion] [nvarchar](max) NULL,
	[Estadogeneraldeboletindenotas] [nvarchar](max) NULL,
	[Totaldehorascalculadasenlinea] [nvarchar](max) NULL,
	[EnlineaTiempoTotal] [nvarchar](max) NULL,
	[Puntuacionmaxima] [nvarchar](max) NULL,
	[Puntuacionenelultimointento] [nvarchar](max) NULL,
	[Puntuacionfinal] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 25/07/2022 6:01:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[employerNumber] [nvarchar](50) NULL,
	[pass] [nvarchar](50) NULL,
	[nombre] [nvarchar](max) NULL,
	[IdRol] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
